/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package boundaries;
import hamkproject.entities.Course;
import hamkproject.entities.CourseTime;
import hamkproject.entities.Department;
import hamkproject.entities.Instructor;
import java.util.ArrayList;
/**
 *
 * @author astahlman
 */
public class CourseBoundary {
    private String name;
    private ArrayList<CourseTime.Days> days;
    private String startTime;
    private String endTime;
    private int creditHours;
    private int capacity;
    private ArrayList<String> prereqCRNS;
    private String semester;
    private String instructorID;
    private String department;
    
    public CourseBoundary(String name, ArrayList<CourseTime.Days> days, String start, String end, 
            int hours, int cap, ArrayList<String> prereqCRNS, String sem, String instructorID,
            String dept)
    {
        this.name = name;
        this.days = days;
        this.startTime = start;
        this.endTime = end;
        this.creditHours = hours;
        this.capacity = cap;
        this.prereqCRNS = prereqCRNS;
        this.semester = sem;
        this.instructorID = instructorID;
        this.department = dept;
    }
    
    public CourseBoundary() {}

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the creditHours
     */
    public int getCreditHours() {
        return creditHours;
    }

    /**
     * @param creditHours the creditHours to set
     */
    public void setCreditHours(int creditHours) {
        this.creditHours = creditHours;
    }

    /**
     * @return the capacity
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * @param capacity the capacity to set
     */
    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    /**
     * @return the semester
     */
    public String getSemester() {
        return semester;
    }

    /**
     * @param semester the semester to set
     */
    public void setSemester(String semester) {
        this.semester = semester;
    }

    /**
     * @return the startTime
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the endTime
     */
    public String getEndTime() {
        return endTime;
    }

    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    /**
     * @return the days
     */
    public ArrayList<CourseTime.Days> getDays() {
        return days;
    }

    /**
     * @param days the days to set
     */
    public void setDays(ArrayList<CourseTime.Days> days) {
        this.days = days;
    }

    /**
     * @return the instructorID
     */
    public String getInstructorID() {
        return instructorID;
    }

    /**
     * @param instructorID the instructorID to set
     */
    public void setInstructorID(String instructorID) {
        this.instructorID = instructorID;
    }

    /**
     * @return the department
     */
    public String getDepartment() {
        return department;
    }

    /**
     * @param department the department to set
     */
    public void setDepartment(String department) {
        this.department = department;
    }

    /**
     * @return the prereqCRNS
     */
    public ArrayList<String> getPrereqCRNS() {
        return prereqCRNS;
    }

    /**
     * @param prereqCRNS the prereqCRNS to set
     */
    public void setPrereqCRNS(ArrayList<String> prereqCRNS) {
        this.prereqCRNS = prereqCRNS;
    }
}
