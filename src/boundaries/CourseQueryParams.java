/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package boundaries;

/**
 *
 * @author astahlman
 */
public class CourseQueryParams {
    private String CRN;
    private String name;
    private String department;
    private String semester;
    private String time;
    private String instructor;

    /**
     * @return the CRN
     */
    public String getCRN() {
        return CRN;
    }

    /**
     * @param CRN the CRN to set
     */
    public void setCRN(String CRN) {
        this.CRN = CRN;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the department
     */
    public String getDepartment() {
        return department;
    }

    /**
     * @param department the department to set
     */
    public void setDepartment(String department) {
        this.department = department;
    }

    /**
     * @return the semester
     */
    public String getSemester() {
        return semester;
    }

    /**
     * @param semester the semester to set
     */
    public void setSemester(String semester) {
        this.semester = semester;
    }

    /**
     * @return the time
     */
    public String getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     * @return the instructor
     */
    public String getInstructor() {
        return instructor;
    }

    /**
     * @param instructor the instructor to set
     */
    public void setInstructor(String instructor) {
        this.instructor = instructor;
    }
    
}
