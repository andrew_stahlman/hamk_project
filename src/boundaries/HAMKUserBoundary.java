/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package boundaries;

import hamkproject.entities.HAMKUser.PermissionEnum;

/**
 *
 * @author astahlman
 */
public class HAMKUserBoundary {
    private String userID;
    private String firstName;
    private String lastName;
    private String department;
    private String password;
    private PermissionEnum permissions;
    
    public HAMKUserBoundary() {}

    /**
     * @return the userID
     */
    public String getUserID() {
        return userID;
    }

    /**
     * @param userID the userID to set
     */
    public void setUserID(String userID) {
        this.userID = userID;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the department
     */
    public String getDepartment() {
        return department;
    }

    /**
     * @param department the department to set
     */
    public void setDepartment(String department) {
        this.department = department;
    }

    /**
     * @return the permissions
     */
    public PermissionEnum getPermissions() {
        return permissions;
    }

    /**
     * @param permissions the permissions to set
     */
    public void setPermissions(PermissionEnum permissions) {
        this.permissions = permissions;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }
}
