/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import hamkproject.HAMKAppFrame;
import hamkproject.HAMKErrorNotice;
import hamkproject.entities.*;
import java.util.ArrayList;
import views.ScheduleUserInterface;

/**
 *
 * @author astahlman
 */
public class ScheduleController extends AbstractController implements IScheduleController {
    
    private ArrayList<Schedule> schedules;
    
    // TODO: Do we need the schedule?
    
    public ScheduleController(HAMKAppFrame frame)
    {
        super(frame);
        HAMKUser user = appFrame.getAuthenticatedUser();
        if (user instanceof Student)
        {
            Student student = (Student) user;
            this.schedules = appFrame.getDatabaseAPI().getAllSchedulesForStudent(student);
        }
        this.view = new ScheduleUserInterface(this);
        
    }
    
    @Override
    public void dropCourse(Course course)
    {
        // drop the course
        HAMKErrorNotice error = new HAMKErrorNotice();
        Student student = (Student) appFrame.getAuthenticatedUser();
        if(!student.dropCourse(course, error))
        {
            view.showAlert(view, error.getMessage());
        }
    }
    
    @Override
    public ArrayList<String> getSemesters()
    {
        ArrayList<String> sems = new ArrayList<String>();
        for (Schedule s : schedules)
        {
            if (!sems.contains(s.getSemester()))
            {
                sems.add(s.getSemester());
            }
        }
        return sems;
    }
    
    @Override
    public Schedule getSchedule(String semester)
    {
        for (Schedule s : schedules)
        {
            if (s.getSemester().equals(semester))
            {
                return s;
            }
        }
        return null;
    }

    @Override
    public ArrayList<Course> getCoursesForInstructor(String semester) 
    {
        Instructor instructor = null;
        ArrayList<Course> courses = new ArrayList<Course>();
        if (appFrame.getAuthenticatedUser() instanceof Instructor)
        {
            instructor = (Instructor)appFrame.getAuthenticatedUser();
        }
        else
        {
            return null;
        }
        
        for (Course c : instructor.getCourses())
        {
            if (c.getSemester().equals(semester))
            {
                courses.add(c);
            }
        }
        
        return courses;
    }
    
    
}
