/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import boundaries.CourseBoundary;
import hamkproject.HAMKAppFrame;
import hamkproject.HAMKErrorNotice;
import hamkproject.entities.Course;
import hamkproject.entities.CourseTime;
import hamkproject.entities.Department;
import hamkproject.entities.Instructor;
import java.util.ArrayList;
import views.NewCourseUserInterface;

/**
 *
 * @author astahlman
 */
public class NewCourseController extends AbstractController implements INewCourseController
{
    public NewCourseController(HAMKAppFrame frame)
    {
        super(frame);
        this.view = new NewCourseUserInterface(this);
    }

    @Override
    public void addCourseToCatalog(CourseBoundary boundary) 
    {
        HAMKErrorNotice error = new HAMKErrorNotice();
        Course course = parseCourseBoundary(boundary, error);
        if (course == null)
        {
            this.view.showAlert(this.view, "Failed to create course: " + error.getMessage());
        }
        else
        {
            String confirmation = "You are about to add a course with the following information:\n";
            confirmation += "Name: " + course.getName() + "\n";
            confirmation += "Instructor: " + course.getInstructor().toString() + "\n";
            confirmation += "Semester: " + course.getSemester() + "\n";
            confirmation += "Credits: " + course.getCreditHours() + "\n";
            confirmation += "Is this information correct?";
            String yes = "Correct";
            String no = "Edit";
            if (this.view.showAlertWithYesNoPrompt(this.view, confirmation, yes, no))
            {
                if (!appFrame.getDatabaseAPI().addCourseToCatalog(course))
                {
                    this.view.showAlert(this.view, "There was an error adding the course to the catalog.");
                }
                else
                {
                    String success = "Added course successfully: CRN \"" + course.getCRN() + "\"\n";
                    success += "Add another course?";
                    if (!this.view.showAlertWithYesNoPrompt(this.view, success, "Yes", "No"))
                    {
                        appFrame.popViewController();
                    }
                    else
                    {
                        NewCourseUserInterface ui = (NewCourseUserInterface)this.view;
                        ui.cleanForm();
                    }
                }
            }
        }
    }
    
    private Course parseCourseBoundary(CourseBoundary boundary, HAMKErrorNotice error)
    {
        if (appFrame.getDatabaseAPI().getCourseByName(boundary.getName()) != null)
        {
            if (error != null)
            {
                error.setMessage("Duplicate course for name \"" + boundary.getName() +"\"");
            }
            return null;
        }
        
        if (boundary.getDays().isEmpty())
        {
            if (error != null)
            {
                error.setMessage("Must specify days.");
            }
            return null;
        }
        // time format is xx:xx 
        else if (boundary.getStartTime().length() != 5 && boundary.getEndTime().length() != 5)
        {
            if (error != null)
            {
                error.setMessage("Invalid start/end time.");
            }
            return null;
        }
        
        CourseTime courseTime = new CourseTime(boundary.getDays(), boundary.getStartTime(), boundary.getEndTime());
        
        Instructor instructor = (Instructor)appFrame.getDatabaseAPI().getUserByID(boundary.getInstructorID());
        if (instructor == null)
        {
            if (error != null)
            {
                error.setMessage("Could not find instructor for ID: \"" + boundary.getInstructorID() + "\"");
            }
            return null;
        }
        
        Department department = appFrame.getDatabaseAPI().getDepartment(boundary.getDepartment());
        if (department == null)
        {
            if (error != null)
            {
                error.setMessage("No such department: \""  + boundary.getDepartment() + "\"");
            }
            return null;
        }
        
        ArrayList<Course> prereqs = new ArrayList<Course>();
        Course prereq;
        for (String crn : boundary.getPrereqCRNS())
        {
            prereq = appFrame.getDatabaseAPI().getCourseByCRN(crn);
            if (prereq != null)
            {
                prereqs.add(prereq);
            }
            else
            {
                if (error != null)
                {
                    error.setMessage("Prereq for CRN: \"" + crn + "\" not found.");
                }
                return null;
            }
        }
        
        // if we've made it this far, course is valid
        // TODO: Add room field to new course form
        return new Course(boundary.getName(), null, boundary.getSemester(), courseTime,
                boundary.getCapacity(), prereqs, instructor, department, boundary.getCreditHours());
    }

    @Override
    public ArrayList<String> getDepartmentNames() {
        ArrayList<String> depts = new ArrayList<String>();
        for (Department d : appFrame.getDatabaseAPI().getAllDepartments())
        {
            depts.add(d.getName());
        }
        return depts;
    }
    

}
