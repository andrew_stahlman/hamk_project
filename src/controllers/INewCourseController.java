/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import boundaries.CourseBoundary;
import java.util.ArrayList;

/**
 *
 * @author astahlman
 */
public interface INewCourseController {
    void addCourseToCatalog(CourseBoundary boundary);
    ArrayList<String> getDepartmentNames();
    /*
        @Override
    public void addCourseToCatalog(CourseBoundary boundary) {
        
        CourseTime courseTime = new CourseTime(boundary.getDays(), boundary.getStartTime(), boundary.getEndTime());
        Instructor instructor = (Instructor)appFrame.getDatabaseAPI().getUserByID(boundary.getInstructorID());
        Department department = appFrame.getDatabaseAPI().getDepartment(boundary.getDepartment());
        ArrayList<Course> prereqs = new ArrayList<Course>();
        Course prereq;
        for (String crn : boundary.getPrereqCRNS())
        {
            prereq = appFrame.getDatabaseAPI().getCourseByCRN(crn);
            if (prereq != null)
            {
                prereqs.add(prereq);
            }
            else
            {
                this.view.showAlert(this.view, "Prereq for CRN: " + crn + " not found. Ignoring.");
            }
        }
        
        Course course = new Course(boundary.getName(), null, boundary.getSemester(), courseTime,
                boundary.getCapacity(), prereqs, instructor, department);
        
       if (!appFrame.getDatabaseAPI().addCourseToCatalog(course))
       {
           this.view.showAlert(this.view, "Course is invalid, could not add to catalog.");
       }
    }
        */
}
