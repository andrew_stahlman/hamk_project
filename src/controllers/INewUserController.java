/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import boundaries.HAMKUserBoundary;
import java.util.ArrayList;

/**
 *
 * @author astahlman
 */
public interface INewUserController {
    void addUserToDatabase(HAMKUserBoundary boundary);
    ArrayList<String> getDepartmentNames();
}
