/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import hamkproject.HAMKAppFrame;
import hamkproject.entities.HAMKUser;
import hamkproject.entities.HAMKUser.PermissionEnum;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import views.MainHubStudentUserInterface;

/**
 *
 * @author astahlman
 */
public class MainHubController extends AbstractController implements IMainHubController {
    
    private static final HashMap<String, Class> CONTROLLER_MAP = createControllerMap(); 
    
    private static HashMap<String, Class> createControllerMap()
    {
        HashMap map = new HashMap<String, Class>();
        map.put(VIEW_SCHEDULE, ScheduleController.class);
        map.put(SEARCH_CATALOG, CourseCatalogController.class);
        map.put(ADD_COURSE, AddCourseController.class);
        map.put(VIEW_ENROLLMENT_SUMMARY, EnrollmentSummaryController.class);
        map.put(ADD_COURSE_TO_CATALOG, NewCourseController.class);
        map.put(ADD_USER_TO_DATABASE, NewUserController.class);
        return map;
    }
    
    public static final String LOGOUT = "Logout";
    public static final String SEARCH_CATALOG = "Search Catalog";
    public static final String VIEW_SCHEDULE = "View Schedule";
    public static final String VIEW_ENROLLMENT_SUMMARY = "View Enrollment Summary";
    public static final String ADD_COURSE = "Add Course";
    public static final String ADD_COURSE_TO_CATALOG = "Add Course To Catalog";
    public static final String ADD_USER_TO_DATABASE = "Add User To Database";
    
    public MainHubController(HAMKAppFrame frame)
    {
        super(frame);
        this.view = new MainHubStudentUserInterface(this);
    }
    
    @Override
    public void logout()
    {
        appFrame.setAuthenticatedUser(null);
    }
    
    @Override
    public void launchController(String actionCommand) 
    {
        // main hub controller lies on top of login controller
        if (actionCommand.equals(LOGOUT))
        {
            appFrame.setAuthenticatedUser(null);
            appFrame.popViewController();
        }
        else
        {
            Class controllerClass = CONTROLLER_MAP.get(actionCommand);
            this.doLaunchController(controllerClass);
        }
    }
    
    private void doLaunchController(Class controllerClass) 
    {
        AbstractController controller = null;
        
        Class[] params = {HAMKAppFrame.class};
        Constructor ctor;
        try {
            ctor = controllerClass.getConstructor(params);
            controller = (AbstractController) ctor.newInstance(new Object[] { this.appFrame });
        }
        catch (Exception e)
        {
            this.view.showAlert(this.view, "Error switching to view.");
        }
        
        /*
        if (controllerClass.equals(ScheduleController.class))
        {
            controller = new ScheduleController(appFrame);
        }
        else if (controllerClass.equals(CourseCatalogController.class))
        {
            controller = new CourseCatalogController(appFrame);
        }
        else if (controllerClass.equals(AddCourseController.class))
        {
            controller = new AddCourseController(appFrame);
        }
        else if (controllerClass.equals(EnrollmentSummaryController.class))
        {
            controller = new EnrollmentSummaryController(appFrame);
        }
        else if (controllerClass.equals(NewCourseController.class))
        {
            controller = new NewCourseController(appFrame);
        }
        else if (controllerClass.equals(NewUserController.class))
        {
            controller = new NewUserController(appFrame);
        }
        else 
        {

        }
        */
        if (controller != null)
        {
            appFrame.pushViewController(controller);
        }
    }
    
}
