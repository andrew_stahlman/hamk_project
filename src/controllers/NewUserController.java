/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import boundaries.HAMKUserBoundary;
import hamkproject.HAMKAppFrame;
import hamkproject.HAMKErrorNotice;
import hamkproject.entities.*;
import hamkproject.entities.HAMKUser.PermissionEnum;
import java.util.ArrayList;
import views.NewUserUserInterface;

/**
 *
 * @author astahlman
 */
public class NewUserController extends AbstractController implements INewUserController 
{

    public NewUserController(HAMKAppFrame frame) {
        super(frame);
        this.view = new NewUserUserInterface(this);
    }

    @Override
    public void addUserToDatabase(HAMKUserBoundary boundary) 
    {
        HAMKErrorNotice error = new HAMKErrorNotice();
        HAMKUser user = parseHAMKUserBoundary(boundary, error);
        if (user == null)
        {
            this.view.showAlert(this.view, "Error adding user: " + error.getMessage());
        }
        else
        {
            String type = "Permissions not set";
            if (user.getModifyPermissions() == PermissionEnum.AdminPermissions)
            {
                type = "Administrator";
            }
            else if (user.getModifyPermissions() == PermissionEnum.InstructorPermissions)
            {
                type = "Instructor";
            }
            else if (user.getModifyPermissions() == PermissionEnum.StudentPermissions)
            {
                type = "Student";
            }
            
            String confirmation = "You are about to add a user with the following information:\n";
            confirmation += "Name: " + user.getFirstName() + " " + user.getLastName() + "\n";
            confirmation += "User ID: " + user.getUserID() + "\n";
            confirmation += "User Classification: " + type + "\n";
            Department d = user.getDepartments().get(0);
            confirmation += "Department: " + d.getName() + "\n";
            confirmation += "Is this information correct?";
            String yes = "Correct";
            String no = "Edit";
            if (this.view.showAlertWithYesNoPrompt(this.view, confirmation, yes, no))
            {
                appFrame.getDatabaseAPI().addUserToDatabase(user);
                String msg = "User added successfully";
                this.view.showAlert(this.view, msg);
                appFrame.popViewController();
            }
        }
    }
    
    private HAMKUser parseHAMKUserBoundary(HAMKUserBoundary boundary, HAMKErrorNotice error)
    {
        HAMKUser theUser = null;
        
        HAMKUser existingUser = appFrame.getDatabaseAPI().getUserByID(boundary.getUserID());
        if (existingUser != null)
        {
            if (error != null)
            {
                error.setMessage("A user with ID \"" + boundary.getUserID() + "\" already exists.");
            }
            return null;
        }
        
        String first = boundary.getFirstName();
        String last = boundary.getLastName();
        String dept = boundary.getDepartment();
        String id = boundary.getUserID();
        String password = boundary.getPassword();
        
        if (boundary.getPermissions() == PermissionEnum.StudentPermissions)
        {
            String sem = appFrame.getDatabaseAPI().getCurrentSemester();
            theUser = new Student(first, last, sem);
            theUser.setModifyPermissions(PermissionEnum.StudentPermissions);
            theUser.setViewPermissions(PermissionEnum.StudentPermissions);
        }
        else if (boundary.getPermissions() == PermissionEnum.AdminPermissions)
        {
            theUser = new Administrator(first, last);
            theUser.setModifyPermissions(PermissionEnum.AdminPermissions);
            theUser.setViewPermissions(PermissionEnum.AdminPermissions);
        }
        else
        {
            theUser = new Instructor(first, last);
            theUser.setModifyPermissions(PermissionEnum.InstructorPermissions);
            theUser.setViewPermissions(PermissionEnum.InstructorPermissions);
        }
        
        theUser.setUserID(id);  
        theUser.setPassword(password);
        
        Department d = appFrame.getDatabaseAPI().getDepartment(dept);
        if (d == null)
        {
            if (error != null)
            {
                error.setMessage("Could not find department \"" + boundary.getDepartment() + "\"");
            }
            
        }
        
        d.addUser(theUser);

        
        return theUser;
    }

    @Override
    public ArrayList<String> getDepartmentNames() {
        ArrayList<String> depts = new ArrayList<String>();
        for (Department d : appFrame.getDatabaseAPI().getAllDepartments())
        {
            depts.add(d.getName());
        }
        return depts;
    }
    
}
