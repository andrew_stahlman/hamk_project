/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import hamkproject.entities.HAMKUser;

/**
 *
 * @author astahlman
 */
public interface IMainHubController {
    void logout();
    void launchController(String actionCommand);
}
