/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import hamkproject.HAMKAppFrame;
import hamkproject.HAMKErrorNotice;
import hamkproject.entities.Course;
import hamkproject.entities.Student;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import views.AddCourseUserInterface;

/**
 *
 * @author astahlman
 */
public class AddCourseController extends AbstractController implements IAddCourseController {
    
    public AddCourseController(HAMKAppFrame frame)
    {
        super(frame);
        this.view = new AddCourseUserInterface(this);
    }
    
    @Override
    public void addCourses(ArrayList<String> crns)
    {
        HashMap<String, HAMKErrorNotice> failures = new HashMap<String, HAMKErrorNotice>();
        Student student = (Student) appFrame.getAuthenticatedUser();
        for (String crn : crns)
        {   
            Course course = appFrame.getDatabaseAPI().getCourseByCRN(crn);
            HAMKErrorNotice error = new HAMKErrorNotice();
            if (course == null)
            {
                failures.put(crn, new HAMKErrorNotice("Course for " + crn + " does not exist."));
            }
            else if (!student.enrollInCourse(course, error))
            {
                failures.put(crn, error);
            }
        }
        
        if (failures.isEmpty())
        {
            this.view.showAlert(this.view, "Courses added successfully.");
        }
        else
        {
            for (Map.Entry<String, HAMKErrorNotice> entry : failures.entrySet())
            {
                String message = entry.getValue().getMessage();
                this.view.showAlert(this.view, message);
            }
        }
    }
}
