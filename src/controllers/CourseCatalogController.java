/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import boundaries.CourseBoundary;
import boundaries.CourseQueryParams;
import hamkproject.HAMKAppFrame;
import hamkproject.HAMKErrorNotice;
import hamkproject.entities.*;
import java.util.ArrayList;
import views.CourseCatalogUserInterface;

/**
 *
 * @author astahlman
 */
public class CourseCatalogController extends AbstractController implements ICourseCatalogController {

    private ArrayList<Course> displayed = new ArrayList<Course>();
    
    public CourseCatalogController(HAMKAppFrame frame)
    {
        super(frame);
        HAMKUser user = appFrame.getAuthenticatedUser();
        this.view = new CourseCatalogUserInterface(this, user.getModifyPermissions());
    }
    

    
    @Override
    public void addCourseToSchedule(Course course) {
        Student student = (Student) appFrame.getAuthenticatedUser();
        HAMKErrorNotice error = new HAMKErrorNotice();
        if (!student.enrollInCourse(course, error))
        {
            this.view.showAlert(this.view, error.getMessage());
        }
        else
        {
        
        }
    }

    @Override
    public ArrayList<Course> doQuery(CourseQueryParams query) {
        displayed = appFrame.getDatabaseAPI().doCourseQuery(query);
        return displayed;
    }

    @Override
    public void removeCourseFromCatalog(Course course) {
        String msg = "Do you really want to permanently delete the course \"" + course.getName() + "\"?";
        if (this.view.showAlertWithYesNoPrompt(this.view, msg, "Yes", "No"))
        {
            if (appFrame.getDatabaseAPI().removeCourseFromCatalog(course))    
            {
                msg = "\"" + course.getName() + "\" removed successfully.";
                displayed.remove(course);
                CourseCatalogUserInterface ui = (CourseCatalogUserInterface)this.view;
                ui.reloadTable(displayed);
            }
            else
            {
                msg = "Could not remove \"" + course.getName() + "\": Course must be empty.";
            }
            this.view.showAlert(this.view, msg);
        }
    }
    
    @Override 
    public ArrayList<Course> getDisplayedCourses()
    {
        return displayed;
    }
}
