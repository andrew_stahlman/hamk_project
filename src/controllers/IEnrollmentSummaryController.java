/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import hamkproject.entities.EnrollmentSummary;
import hamkproject.entities.Registration;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author astahlman
 */
public interface IEnrollmentSummaryController {
    public HashMap<Registration, EnrollmentSummary.Grade> getAllRegisteredCourses(String semester);
    public ArrayList<String> getSemesters();
}
