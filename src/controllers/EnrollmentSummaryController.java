/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import hamkproject.HAMKAppFrame;
import hamkproject.entities.EnrollmentSummary;
import hamkproject.entities.EnrollmentSummary.Grade;
import hamkproject.entities.Registration;
import hamkproject.entities.Student;
import java.util.ArrayList;
import java.util.HashMap;
import views.EnrollmentSummaryUserInterface;

/**
 *
 * @author astahlman
 */
public class EnrollmentSummaryController extends AbstractController implements IEnrollmentSummaryController
{
    private EnrollmentSummary enrollmentSummary;
    
    public EnrollmentSummaryController(HAMKAppFrame frame)
    {
        super(frame);
        Student student = (Student) appFrame.getAuthenticatedUser();
        this.enrollmentSummary = student.getEnrollmentSummary();
        this.view = new EnrollmentSummaryUserInterface(this);
    }
    
    @Override
    public HashMap<Registration, Grade> getAllRegisteredCourses(String semester) {
        HashMap<Registration, Grade> courses = new HashMap<Registration, Grade>();
        for (Registration r : enrollmentSummary.getCourseHistory().keySet())
        {
            if (r.getSemester().equals(semester))
            {
                Grade grade = enrollmentSummary.getCourseHistory().get(r);
                courses.put(r, grade);
            }
        }
        return courses;
    }

    @Override
    public ArrayList<String> getSemesters() {
        ArrayList<String> semesters = new ArrayList<String>();
        for (Registration r : this.enrollmentSummary.getCourseHistory().keySet())
        {
            if (!semesters.contains(r.getSemester()))
            {
                semesters.add(r.getSemester());
            }
        }
        
        return semesters;
    }
   
}
