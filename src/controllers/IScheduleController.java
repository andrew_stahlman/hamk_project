/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import hamkproject.entities.Course;
import hamkproject.entities.Schedule;
import java.util.ArrayList;

/**
 *
 * @author astahlman
 */
public interface IScheduleController {
    void dropCourse(Course course);
    ArrayList<String> getSemesters();
    Schedule getSchedule(String semester);
    ArrayList<Course> getCoursesForInstructor(String semester);
}
