/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import boundaries.CourseBoundary;
import controllers.AbstractController;
import controllers.INewCourseController;
import hamkproject.entities.CourseTime;
import java.util.ArrayList;
import javax.swing.SpinnerNumberModel;

/**
 *
 * @author astahlman
 */
public class NewCourseUserInterface extends AbstractUserInterface {

    INewCourseController controller;
    
    public NewCourseUserInterface(INewCourseController controller)
    {
        this.controller = controller;
        initComponents();
        this.departmentComboBox.removeAllItems();
        for (String dept : controller.getDepartmentNames())
        {
            this.departmentComboBox.addItem(dept);
        }
    }
    
    @Override
    public void setController(AbstractController controller)
    {
        this.controller = (INewCourseController) controller;
    }
    
    @Override
    public AbstractController getController()
    {
        return (AbstractController) this.controller;
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        courseNameTextField = new javax.swing.JTextField();
        mondayCheckBox = new javax.swing.JCheckBox();
        tuesdayCheckBox = new javax.swing.JCheckBox();
        wednesdayCheckBox = new javax.swing.JCheckBox();
        thursdayCheckBox = new javax.swing.JCheckBox();
        fridayCheckBox = new javax.swing.JCheckBox();
        SpinnerNumberModel hourModel1 = new SpinnerNumberModel(8, 7, 22, 1);
        startHourSpinner = new javax.swing.JSpinner();
        SpinnerNumberModel minutesModel1 = new SpinnerNumberModel(0, 0, 55, 5);
        startMinuteSpinner = new javax.swing.JSpinner();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        SpinnerNumberModel hourModel2 = new SpinnerNumberModel(8, 7, 22, 1);
        endHourSpinner = new javax.swing.JSpinner();
        SpinnerNumberModel minutesModel2 = new SpinnerNumberModel(0, 0, 55, 5);
        endMinuteSpinner = new javax.swing.JSpinner();
        departmentComboBox = new javax.swing.JComboBox();
        jLabel5 = new javax.swing.JLabel();
        SpinnerNumberModel creditModel = new SpinnerNumberModel(3, 0, 25, 1);
        creditHoursSpinner = new javax.swing.JSpinner();
        jLabel6 = new javax.swing.JLabel();
        instructorIDTextField = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        semesterTextField = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        prereq1TextField = new javax.swing.JTextField();
        prereq2TextField = new javax.swing.JTextField();
        prereq3TextField = new javax.swing.JTextField();
        submitButton = new javax.swing.JButton();
        capacityTextField = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();

        mondayCheckBox.setText("M");

        tuesdayCheckBox.setText("T");

        wednesdayCheckBox.setText("W");

        thursdayCheckBox.setText("TH");

        fridayCheckBox.setText("F");

        startHourSpinner.setModel(hourModel1);

        startMinuteSpinner.setModel(minutesModel1);

        jLabel1.setText("Course Name:");

        jLabel2.setText("Days Met:");

        jLabel3.setText("Start Time:");

        jLabel4.setText("End Time:");

        endHourSpinner.setModel(hourModel2);

        endMinuteSpinner.setModel(minutesModel2);

        departmentComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel5.setText("Department:");

        creditHoursSpinner.setModel(creditModel);

        jLabel6.setText("Credit Hours");

        jLabel7.setText("Instructor ID:");

        jLabel8.setText("Semester:");

        jLabel9.setText("Prerequisite CRN:");

        submitButton.setText("Submit");
        submitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitButtonActionPerformed(evt);
            }
        });

        jLabel10.setText("Capacity:");

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(81, 81, 81)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(27, 27, 27)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(jLabel2)
                            .add(jLabel1)
                            .add(jLabel3)
                            .add(layout.createSequentialGroup()
                                .add(jLabel5)
                                .add(2, 2, 2))
                            .add(layout.createSequentialGroup()
                                .add(jLabel6)
                                .add(4, 4, 4)))
                        .add(27, 27, 27)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(layout.createSequentialGroup()
                                .add(mondayCheckBox)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                                .add(tuesdayCheckBox)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(wednesdayCheckBox)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                                .add(thursdayCheckBox)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(fridayCheckBox))
                            .add(courseNameTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 251, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(layout.createSequentialGroup()
                                .add(6, 6, 6)
                                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(departmentComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .add(layout.createSequentialGroup()
                                        .add(startHourSpinner, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .add(12, 12, 12)
                                        .add(startMinuteSpinner, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .add(18, 18, 18)
                                        .add(jLabel4)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                                        .add(endHourSpinner, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                                        .add(endMinuteSpinner, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                    .add(creditHoursSpinner, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))))
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(jLabel8)
                            .add(jLabel9)
                            .add(jLabel10)
                            .add(jLabel7))
                        .add(33, 33, 33)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                .add(instructorIDTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 144, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(semesterTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 144, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(layout.createSequentialGroup()
                                .add(capacityTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 51, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(85, 85, 85)
                                .add(submitButton))
                            .add(layout.createSequentialGroup()
                                .add(prereq1TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 75, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(prereq2TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 75, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(prereq3TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 75, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(82, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(courseNameTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel1))
                .add(2, 2, 2)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(mondayCheckBox)
                    .add(tuesdayCheckBox)
                    .add(wednesdayCheckBox)
                    .add(thursdayCheckBox)
                    .add(fridayCheckBox)
                    .add(jLabel2))
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(12, 12, 12)
                        .add(jLabel3))
                    .add(layout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(startMinuteSpinner, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(startHourSpinner, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                                    .add(jLabel4)
                                    .add(6, 6, 6)))
                            .add(endHourSpinner, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(endMinuteSpinner, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel5)
                    .add(departmentComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(creditHoursSpinner, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel6))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(instructorIDTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel7))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(semesterTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel8))
                .add(2, 2, 2)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(prereq1TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(prereq2TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(prereq3TextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel9))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(capacityTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel10)
                    .add(submitButton))
                .add(0, 15, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void submitButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submitButtonActionPerformed
        // TODO add your handling code here:
        CourseBoundary boundary = new CourseBoundary();
        
        // name
        if (!courseNameTextField.getText().isEmpty())
        {
            boundary.setName(courseNameTextField.getText());
        }
           
        // days
        boundary.setDays(getDays());
        
        // credit hours
        SpinnerNumberModel model = (SpinnerNumberModel) creditHoursSpinner.getModel();
        int credit = model.getNumber().intValue();
        boundary.setCreditHours(credit);
        
        // start time
        model = (SpinnerNumberModel) startHourSpinner.getModel();
        int hour = model.getNumber().intValue();
        model = (SpinnerNumberModel) startMinuteSpinner.getModel();
        int minute = model.getNumber().intValue();
        String startHour = String.format("%02d", hour);
        String startMinute = String.format("%02d", minute);
        
        String startTime = startHour + ":" + startMinute;
        
        // end time
        model = (SpinnerNumberModel) endHourSpinner.getModel();
        hour = model.getNumber().intValue();
        model = (SpinnerNumberModel) endMinuteSpinner.getModel();
        minute = model.getNumber().intValue();
        String endHour = String.format("%02d", hour);
        String endMinute = String.format("%02d", minute);
        
        String endTime = endHour + ":" + endMinute;
        
        boundary.setStartTime(startTime);
        boundary.setEndTime(endTime);
        
        // department
        String dept = (String) departmentComboBox.getSelectedItem();
        boundary.setDepartment(dept);
        
        // instructor
        String instructorID = instructorIDTextField.getText();
        boundary.setInstructorID(instructorID);
        
        // capacity
        int capacity;
        try
        {
            capacity = Integer.parseInt(capacityTextField.getText());
        }
        catch (NumberFormatException e)
        {
            capacity = 50;
        }
        boundary.setCapacity(capacity);
        
        // prereqs
        ArrayList<String> prereqCRNS = new ArrayList<String>();
        if (!prereq1TextField.getText().isEmpty())
        {
            prereqCRNS.add(prereq1TextField.getText());
        }
        if (!prereq2TextField.getText().isEmpty())
        {
            prereqCRNS.add(prereq2TextField.getText());
        }
        if (!prereq3TextField.getText().isEmpty())
        {
            prereqCRNS.add(prereq3TextField.getText());
        }
        boundary.setPrereqCRNS(prereqCRNS);
        
        // semester
        String sem = semesterTextField.getText();
        boundary.setSemester(sem);
        
        controller.addCourseToCatalog(boundary); 
    }//GEN-LAST:event_submitButtonActionPerformed

    private ArrayList<CourseTime.Days> getDays()
    {
        ArrayList<CourseTime.Days> days = new ArrayList<CourseTime.Days>();
        
        if (mondayCheckBox.isSelected())
        {
            days.add(CourseTime.Days.M);
        }
        if (tuesdayCheckBox.isSelected())
        {
            days.add(CourseTime.Days.T);
        }
        if (wednesdayCheckBox.isSelected())
        {
            days.add(CourseTime.Days.W);
        }
        if (thursdayCheckBox.isSelected())
        {
            days.add(CourseTime.Days.H);
        }
        if (fridayCheckBox.isSelected())
        {
            days.add(CourseTime.Days.F);
        }
        
        return days;
    }
    
    public void cleanForm()
    {
        capacityTextField.setText("");
        courseNameTextField.setText("");
        instructorIDTextField.setText("");
        prereq1TextField.setText("");
        prereq2TextField.setText("");
        prereq3TextField.setText("");
        mondayCheckBox.setSelected(false);
        tuesdayCheckBox.setSelected(false);
        wednesdayCheckBox.setSelected(false);
        thursdayCheckBox.setSelected(false);
        fridayCheckBox.setSelected(false);
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField capacityTextField;
    private javax.swing.JTextField courseNameTextField;
    private javax.swing.JSpinner creditHoursSpinner;
    private javax.swing.JComboBox departmentComboBox;
    private javax.swing.JSpinner endHourSpinner;
    private javax.swing.JSpinner endMinuteSpinner;
    private javax.swing.JCheckBox fridayCheckBox;
    private javax.swing.JTextField instructorIDTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JCheckBox mondayCheckBox;
    private javax.swing.JTextField prereq1TextField;
    private javax.swing.JTextField prereq2TextField;
    private javax.swing.JTextField prereq3TextField;
    private javax.swing.JTextField semesterTextField;
    private javax.swing.JSpinner startHourSpinner;
    private javax.swing.JSpinner startMinuteSpinner;
    private javax.swing.JButton submitButton;
    private javax.swing.JCheckBox thursdayCheckBox;
    private javax.swing.JCheckBox tuesdayCheckBox;
    private javax.swing.JCheckBox wednesdayCheckBox;
    // End of variables declaration//GEN-END:variables
}
