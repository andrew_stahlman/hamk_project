/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package views.tablemodels;

import hamkproject.entities.Course;
import java.util.ArrayList;

/**
 *
 * @author astahlman
 */
public class CourseListTableModel extends CustomTableModel
{
    public CourseListTableModel(ArrayList elements)
    {
        super(elements, new String[] {"Course", "Time", "Credit Hours"});
        this.loadTableData();
    }
    
    @Override
    protected void loadTableData()
    {
        short row = 0;
        for (Object el : getElements())
        {
            Course c = (Course) el;
            if (row < MAX_ROWS)
            {
                data[row][0] = c.getName();
                data[row][1] = c.getTimeMet().toString();
                data[row][2] = c.getCreditHours();
                row++;
            }
        }
        super.loadTableData();
    }
}
