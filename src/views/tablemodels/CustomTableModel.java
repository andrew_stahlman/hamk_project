/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package views.tablemodels;

import java.util.ArrayList;
import java.util.Collection;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author astahlman
 */
public abstract class CustomTableModel extends AbstractTableModel
{
    protected String columnNames[];
    protected ArrayList elements;
    protected Object[][] data;

    
    protected static final short MAX_ROWS = 100;
    public CustomTableModel(ArrayList elements, String[] columnNames)
    {
        this.elements = elements;
        this.columnNames = columnNames;
        data = new Object[MAX_ROWS][columnNames.length];
    }
    
    protected void loadTableData()
    {
        this.fireTableDataChanged();
    }
    
    /**
     * @return the elements
     */
    public Collection getElements() {
        return elements;
    }

    /**
     * @param elements the elements to set
     */
    public void setElements(ArrayList elements) {
        this.elements = elements;
    }
    
    @Override
    public Object getValueAt(int row, int col)
    {
        return data[row][col];
    }
    
    @Override
    public int getColumnCount()
    {
        return columnNames.length;
    }
    
    @Override
    public String getColumnName(int col)
    {
        return columnNames[col];
    }
    
    @Override
    public int getRowCount()
    {
        return getElements().size();
    }
    
    @Override
    public Class getColumnClass(int c) 
    {
        Object val = getValueAt(0,c);
        if (val != null)
        {
            return val.getClass();
        }
        return null;
    }
    
    @Override
    public boolean isCellEditable(int row, int col)
    {
        return false;
    }
    
    public void onElementsChanged()
    {
        loadTableData();
    }

    public Object getElementForRow(int row)
    {
        return elements.get(row);
    }
}
