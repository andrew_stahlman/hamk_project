/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package views.tablemodels;

import java.util.ArrayList;

/**
 *
 * @author astahlman
 */
public abstract class CustomTableModelWithButton extends CustomTableModel {
    private String buttonString;
    
    public CustomTableModelWithButton(ArrayList elements, String[] columnNames)
    {
        super(elements, columnNames);
        this.buttonString = columnNames[columnNames.length - 1];
    }
    
    @Override
    protected void loadTableData()
    {
        for (int i = 0; i < getRowCount(); i++)
        {
            int col = getColumnCount() - 1;
            data[i][col] = buttonString;
        }
        super.loadTableData();
    }
    
    @Override
    public Class getColumnClass(int c) {
        
        if (c == (getColumnCount() - 1))
        {
            return ButtonCellEditor.class;
        }
        
        return getValueAt(0, c).getClass();
    }
    
    @Override
    public boolean isCellEditable(int row, int col)
    {
        if (col == (getColumnCount() - 1))
        {
            return true;
        }
        return false;
    }
}
