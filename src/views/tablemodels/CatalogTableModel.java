/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package views.tablemodels;

import hamkproject.entities.Course;
import java.util.ArrayList;

/**
 *
 * @author astahlman
 */
public class CatalogTableModel extends CustomTableModelWithButton {
    public CatalogTableModel(ArrayList courses)
    {
        super(courses, new String[] {"CRN", "Course", "Time", "Credit Hours", "Instructor", "Add"});
        this.loadTableData();
    }
    
    @Override
    protected void loadTableData()
    {
        short row = 0;
        for (Object el : getElements())
        {
            Course c = (Course) el;
            if (row < MAX_ROWS)
            {
                data[row][0] = c.getCRN();
                data[row][1] = c.getName();
                data[row][2] = c.getTimeMet().toString(); 
                data[row][3] = c.getCreditHours();
                data[row][4] = c.getInstructor().toString();
                row++;
            }
        }
        super.loadTableData();
    }
    
}
