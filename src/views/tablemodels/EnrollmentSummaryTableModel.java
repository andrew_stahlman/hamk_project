/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package views.tablemodels;

import hamkproject.entities.Course;
import hamkproject.entities.EnrollmentSummary;
import hamkproject.entities.Registration;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author astahlman
 */
public class EnrollmentSummaryTableModel extends CustomTableModel {
    
    HashMap<Registration, EnrollmentSummary.Grade> gradeMap;
    
    public EnrollmentSummaryTableModel(HashMap<Registration, EnrollmentSummary.Grade> gradeMap)
    {
        super(new ArrayList(gradeMap.keySet()), new String[] {"Name", "Registered On", "Instructor", "Grade"});
        this.gradeMap = gradeMap;
        loadTableData();
    }
    
    @Override
    public void loadTableData()
    {
        short row = 0;
        for (Object el : this.elements)
        {
            Registration r = (Registration) el;
            Course c = r.getCourse();
            data[row][0] = c.getName();
            SimpleDateFormat formatter = new SimpleDateFormat("MMM d, ''yy");
            data[row][1] = formatter.format(r.getRegistrationTime());
            data[row][2] = c.getInstructor().toString();
            String gradeString;
            EnrollmentSummary.Grade grade = gradeMap.get(r);
            if (grade != null)
            {
                gradeString = grade.name();
            }
            else
            {
                gradeString = "-";
            }
            data[row][3] = gradeString;
            row++;
        }
        super.loadTableData();
    }
    
    public void reloadTable(HashMap<Registration, EnrollmentSummary.Grade> gradeMap)
    {
        this.gradeMap = gradeMap;
        this.elements = new ArrayList(gradeMap.keySet());
        loadTableData();
    }
}
