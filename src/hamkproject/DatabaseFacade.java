/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hamkproject;

import boundaries.CourseQueryParams;
import hamkproject.entities.*;
import java.util.ArrayList;

/**
 *
 * @author astahlman
 */
public class DatabaseFacade implements DatabaseAPI {

    // dbsession variable here
    DatabaseAPI dbAPI;
    
    public DatabaseFacade(DatabaseAPI apiInstance)
    {
        dbAPI = apiInstance;
    }

    @Override
    public HAMKUser authenticateUser(String userID, String password) {
        return dbAPI.authenticateUser(userID, password);
    }

    @Override
    public Course getCourseByCRN(String crn) {
        return dbAPI.getCourseByCRN(crn);
    }

    @Override
    public ArrayList<Course> getCoursesForInstructor(Instructor instructor) {
        return dbAPI.getCoursesForInstructor(instructor);
    }

    @Override
    public ArrayList<Schedule> getAllSchedulesForStudent(Student student) {
        return dbAPI.getAllSchedulesForStudent(student);
    }

    @Override
    public HAMKUser getUserByID(String id) {
        return dbAPI.getUserByID(id);
    }
    
    @Override
    public boolean addCourseToCatalog(Course course) {
        return dbAPI.addCourseToCatalog(course);
    }

    @Override
    public ArrayList<Course> doCourseQuery(CourseQueryParams query) {
        return dbAPI.doCourseQuery(query);
    }
    
    
    @Override
    public boolean removeCourseFromCatalog(Course course) {
        return dbAPI.removeCourseFromCatalog(course);
    }
    
    @Override
    public String getCurrentSemester()
    {
        return dbAPI.getCurrentSemester();
    }
    
    @Override
    public ArrayList<Department> getAllDepartments()
    {
        return dbAPI.getAllDepartments();
    }

    @Override
    public Department getDepartment(String deptName) {
        return dbAPI.getDepartment(deptName);
    }

    @Override
    public Course getCourseByName(String name) {
        return dbAPI.getCourseByName(name);
    }

    @Override
    public boolean addUserToDatabase(HAMKUser user) {
        return dbAPI.addUserToDatabase(user);
    }
    
    
}
