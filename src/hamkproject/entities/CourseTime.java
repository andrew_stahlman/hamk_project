/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hamkproject.entities;

import java.util.ArrayList;

/**
 *
 * @author astahlman
 */
public class CourseTime extends HAMKEntity {


    public enum Days { M, T, W, H, F }
    private ArrayList<Days> days;
    private String startTime;
    private String endTime;
    


    /**
     * @return the startTime
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the endTime
     */
    public String getEndTime() {
        return endTime;
    }

    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        String daysString = "";
        for (Days d : getDays())
        {
            daysString += d.name();
        }
        return daysString + " " + startTime + " - " + endTime;
    }
    
        /**
     * @return the days
     */
    public ArrayList<Days> getDays() {
        return days;
    }

    /**
     * @param days the days to set
     */
    public void setDays(ArrayList<Days> days) {
        this.days = days;
    }
    
    public CourseTime(ArrayList<Days> days, String startTime, String endTime)
    {
        this.days = days;
        this.startTime = startTime;
        this.endTime = endTime;
    }
}
