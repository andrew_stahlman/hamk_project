/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hamkproject.entities;

import hamkproject.HAMKErrorNotice;
import java.util.ArrayList;

/**
 *
 * @author astahlman
 */

public class Schedule extends HAMKEntity {

    private ArrayList<Course> courses;
    private Student student;
    private String semester;
    public static final int MAX_HOURS = 18;


    @Override
    public String toString() {
        return student.getFirstName() + semester;
    }

    /**
     * @return the courses
     */
    public ArrayList<Course> getCourses() {
        return courses;
    }

    /**
     * @param courses the courses to set
     */
    public void setCourses(ArrayList<Course> courses) {
        this.courses = courses;
    }

    /**
     * @return the student
     */
    public Student getStudent() {
        return student;
    }

    /**
     * @param student the student to set
     */
    public void setStudent(Student student) {
        this.student = student;
    }
    
    
    /**
     * @return the semester
     */
    public String getSemester() {
        return semester;
    }

    /**
     * @param semester the semester to set
     */
    public void setSemester(String semester) {
        this.semester = semester;
    }
    
    public Schedule(Student student, String sem)
    {
        this.student = student;
        this.semester = sem;
        this.courses = new ArrayList<Course>();
    }
    
    public Boolean addCourse(Course course, HAMKErrorNotice error)
    {
        short hours = 0;
        for (Course c : courses)
        {
            hours += c.getCreditHours();
        }
        int newHours = hours + course.getCreditHours();
        
        if (hours + course.getCreditHours() > MAX_HOURS)
        {
            if (error != null)
            {
                error.setMessage("Could not add \"" + course.getName() + "\" to schedule: Too many hours. (" + newHours + ")");
            }
            return false;
        }

        /*
        Department dept = course.getDepartment();
        if (!dept.getCurrentSemester().equals(course.getSemester()))
        {
            if (error != null)
            {
                error.setMessage("Could not add \"" + course.getName() + "\" to schedule: Not offered this semester.");
            }
            return false;
        }
        */
        if (checkConflicts(course))
        {
            if (error != null)
            {
                error.setMessage("Could not add \"" + course.getName() + "\" to schedule: Course conflicts with schedule.");
            }
            return false;
        }
        
        for (Course prereq : course.getPrereqs())
        {
            if (!student.hasCompletedCourse(prereq))
            {
                if (error != null)
                {
                    error.setMessage("Could not add \"" + course.getName() + "\" to schedule: Missing prereq \"" + prereq.getName() +"\"");
                }
                return false;
            }
        }
        
        if (courses.contains(course))
        {
            if (error != null)
            {
                error.setMessage("Already registered for \"" + course.getName() + "\"");
            }
            return false;
        }
        
        if (course.addStudent(student, error))
        {
            courses.add(course);
            return true;
        }
        
        return false;
    }
    
    public Boolean dropCourse(Course course, HAMKErrorNotice error)
    {
        if (!courses.contains(course))
        {
            if (error != null)
            {
                error.setMessage("Could not drop course: Not enrolled in \"" + course.getName() + "\"");
            }
            return false;
        }
        
        if (course.removeStudent(student))
        {
            courses.remove(course);
            return true;
        }

        return false;
    }

    /*
    public boolean completeCourse(Course c, HAMKErrorNotice error)
    {
        if (courses.remove(c))
        {
            return true;
        }
        else
        {
            if (error != null)
            {
                error.setMessage("Could not complete " + c.getName() +" - not enrolled.");
            }
            return false;
        }
    }
    */
    private boolean checkConflicts(Course newCourse)
    {
        // each course in the schedule
        for (Course c : getCourses())
        {
            ArrayList<CourseTime.Days> days = c.getTimeMet().getDays();
            // each day in the scheduled course
            for (CourseTime.Days d1 : days)
            {
                // each day in the new course
                for (CourseTime.Days d2 : newCourse.getTimeMet().getDays())
                {
                    String start1 = c.getTimeMet().getStartTime();
                    String end1 = c.getTimeMet().getEndTime();
                    String start2 = newCourse.getTimeMet().getStartTime();
                    String end2 = newCourse.getTimeMet().getEndTime();
                    if (d1.equals(d2) && checkTimesOverlap(start1, end1, start2, end2))
                    {
                        return true;
                    }
                }
            }
        }
        
        return false;
    }
    
    private boolean checkTimesOverlap(String start1, String end1, String start2, String end2)
    {
        int s1 = Integer.parseInt(start1.substring(0, 2));
        int e1 = Integer.parseInt(end1.substring(0, 2));
        int s2 = Integer.parseInt(start2.substring(0, 2));
        int e2 = Integer.parseInt(end2.substring(0, 2));
        
        if (s2 >= s1 && s2 <= e1)
        {
            int endMinutes1 = Integer.parseInt(end1.substring(end1.length() - 2, end1.length()));
            int startMinutes2 = Integer.parseInt(start2.substring(start1.length() - 2, start2.length()));
            if (startMinutes2 <= endMinutes1)
            {
                return true;
            }
        }
        else if (s1 >= s2 && s1 <= e2)
                    {
            int endMinutes2 = Integer.parseInt(end2.substring(end2.length() - 2, end2.length()));
            int startMinutes1 = Integer.parseInt(start1.substring(start1.length() - 2, start1.length()));
            if (startMinutes1 <= endMinutes2)
            {
                return true;
            }
        }
        
        return false;
    }
    
}
