/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hamkproject.entities;


import hamkproject.HAMKErrorNotice;
import java.util.HashMap;
/**
 *
 * @author astahlman
 */
public class EnrollmentSummary extends HAMKEntity {

    private Student student;

    /**
     * @return the courseHistory
     */
    public HashMap<Registration, Grade> getCourseHistory() {
        return courseHistory;
    }

    /**
     * @param courseHistory the courseHistory to set
     */
    public void setCourseHistory(HashMap<Registration, Grade> courseHistory) {
        this.courseHistory = courseHistory;
    }

    public enum Grade { A, B, C, D, F, DroppedNoPenalty, DroppedFail};
    
    private HashMap<Registration, Grade> courseHistory = new HashMap<Registration, Grade>();
    
    
    public EnrollmentSummary(Student student)
    {
        this.student = student;
    }
    
    public boolean hasCompletedCourse(Course course)
    {
        for (Registration r : getCourseHistory().keySet())
        {
            Course c = r.getCourse();
            if (c.equals(course))
            {
                Grade grade = getCourseHistory().get(r);
                return (grade == Grade.A || grade == Grade.B || grade == Grade.C);
            }
        }
        
        return false;
    }
    
    public boolean addRegistration(Registration r)
    {
        if (r != null && getCourseHistory().containsKey(r) == false)
        {
            r.setEnrollmentSummary(this);
            getCourseHistory().put(r, null);
            return true;
        }
        return false;
    }
    
    public boolean completeCourse(Course course, Grade grade, HAMKErrorNotice error)
    {
        for (Registration r : getCourseHistory().keySet())
        {
            if (r.getCourse().equals(course))
            {
                getCourseHistory().put(r, grade);
                return true;
            }
        }
        
        error.setMessage("Not enrolled in " + course.getName());
        return false;
    }
    
    public boolean dropCourse(Course course, HAMKErrorNotice error)
    {
        for (Registration r : getCourseHistory().keySet())
        {
            if (r.getCourse().equals(course))
            {
                getCourseHistory().put(r, Grade.DroppedNoPenalty);
                return true;
                // TODO: Add check for end of semester
            }
        }
        
        error.setMessage("Not enrolled in " + course.getName());
        return false;
    }
    
}
