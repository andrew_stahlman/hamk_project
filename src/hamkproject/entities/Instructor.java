/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hamkproject.entities;

import hamkproject.HAMKErrorNotice;
import java.util.ArrayList;
/**
 *
 * @author astahlman
 */

public class Instructor extends HAMKUser {

    
    private ArrayList<Course> courses = new ArrayList<Course>();
    
    public Instructor(String firstName, String lastName)
    {
        super(firstName, lastName);
        viewPermissions = PermissionEnum.InstructorPermissions;
        modifyPermissions = PermissionEnum.InstructorPermissions;
    }
    
    public void addCourse(Course course)
    {
        if (course != null && !(courses.contains(course)))
        {
            getCourses().add(course);
        }
    }
    
    public void assignGrade(Student student, Course course, EnrollmentSummary.Grade grade, HAMKErrorNotice error)
    {
        student.completeCourse(course, grade, error);
    }

    /**
     * @return the courses
     */
    public ArrayList<Course> getCourses() {
        return courses;
    }

    /**
     * @param courses the courses to set
     */
    public void setCourses(ArrayList<Course> courses) {
        this.courses = courses;
    }
    
}
