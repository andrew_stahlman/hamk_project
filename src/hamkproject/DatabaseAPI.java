/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hamkproject;

import boundaries.CourseQueryParams;
import hamkproject.entities.*;
import java.util.ArrayList;

/**
 *
 * @author astahlman
 */
public interface DatabaseAPI {
    HAMKUser getUserByID(String id);
    Course getCourseByCRN(String crn);
    Course getCourseByName(String name);
    ArrayList<Schedule> getAllSchedulesForStudent(Student student);
    ArrayList<Course> getCoursesForInstructor(Instructor instructor);
    HAMKUser authenticateUser(String userID, String password);
    boolean addCourseToCatalog(Course course);
    boolean removeCourseFromCatalog(Course course);
    ArrayList<Course> doCourseQuery(CourseQueryParams query);
    String getCurrentSemester();
    ArrayList<Department> getAllDepartments();
    Department getDepartment(String deptName);
    boolean addUserToDatabase(HAMKUser user);
}
