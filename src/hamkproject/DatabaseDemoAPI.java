/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hamkproject;

import boundaries.CourseQueryParams;
import hamkproject.entities.CourseTime.Days;
import hamkproject.entities.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

/**
 *
 * @author astahlman
 */
public class DatabaseDemoAPI implements DatabaseAPI
{
    
    private University university = new University("HAMK U.", "Fall 2012");

    private ArrayList<HAMKUser> userDatabase = new ArrayList<HAMKUser>();
    private ArrayList<Course> courseCatalog = new ArrayList<Course>();
    private ArrayList<Schedule> schedules = new ArrayList<Schedule>();
    private ArrayList<Department> departments = new ArrayList<Department>();
    //private ArrayList<EnrollmentSummary> enrollmentSummaries = new ArrayList<EnrollmentSummary>();
    
    
    
    public static final String STUDENT_ID = "Test_Student";
    public static final String ADMIN_ID = "Test_Admin";
    public static final String INSTRUCTOR_ID = "Test_Instructor";
    

    
    public static final int DEFAULT_CAPACITY = 50;
    public static final int DEFAULT_CREDITS = 3;
            
    public DatabaseDemoAPI()
    {
        Department swenDept = new Department(university, "SWEN");
        Department englDept = new Department(university, "ENGL");
        
        departments.add(swenDept);
        departments.add(englDept);
        
        // Create users: student, admin and instructor
        Student testStudent = new Student("Test", "Student", university.getSemester());
        testStudent.setUserID(STUDENT_ID);
        testStudent.setPassword("");
            
        Administrator testAdmin = new Administrator("Test", "Admin");
        testAdmin.setUserID(ADMIN_ID);
        testAdmin.setPassword("");
        
        Instructor testInstructor = new Instructor("Test", "Instructor");
        testInstructor.setUserID(INSTRUCTOR_ID);
        testInstructor.setPassword("");
        
        swenDept.addUser(testInstructor);
        swenDept.addUser(testStudent);
        
        userDatabase.add(testStudent);
        userDatabase.add(testAdmin);
        userDatabase.add(testInstructor);
                
        ArrayList<Days> mwfDays = new ArrayList<Days>();
        mwfDays.add(Days.M);
        mwfDays.add(Days.W);
        mwfDays.add(Days.F);
        
        CourseTime testTime = new CourseTime(mwfDays, "13:00", "13:50");
        
        // create an old test course
        Course oldCourse = new Course("Bob the Builder and the Human Odyssey", "Shelby 3.14",
                "Fall 2011", testTime, DEFAULT_CAPACITY, null, testInstructor, englDept, DEFAULT_CREDITS);
        
        ArrayList<Course> prereqs = new ArrayList<Course>();
        prereqs.add(oldCourse);
        
        // Current courses
        Course testCourse = new Course("Underwater Basket Weaving", 
                "Shelby 5 and 3/4", university.getSemester(), testTime, DEFAULT_CAPACITY, 
                prereqs, testInstructor, swenDept, DEFAULT_CREDITS);
        
        CourseTime testTime2 = new CourseTime(mwfDays, "13:30", "14:45");
        // should conflict with testCourse
        Course testCourse2 = new Course("Mayan Pottery", 
                "Shelby 360", university.getSemester(), testTime2, DEFAULT_CAPACITY, 
                prereqs, testInstructor, swenDept, DEFAULT_CREDITS);

        englDept.addCourse(oldCourse);
        swenDept.addCourse(testCourse);
        swenDept.addCourse(testCourse2);
        
        this.addCourseToCatalog(oldCourse);
        this.addCourseToCatalog(testCourse);
        this.addCourseToCatalog(testCourse2);
        
        
        // save the student's schedule
        Schedule studentSchedule = testStudent.getSchedule();
        
        // add an old schedule
        Schedule oldSchedule = new Schedule(testStudent, "Fall 2011");
        testStudent.setSchedule(oldSchedule);
        HAMKErrorNotice error = new HAMKErrorNotice();
        testStudent.enrollInCourse(oldCourse, error);
        testStudent.completeCourse(oldCourse, EnrollmentSummary.Grade.A, error);

        // set the schedule back and enroll in the test course
        testStudent.setSchedule(studentSchedule);
        testStudent.enrollInCourse(testCourse, error);
        
        this.schedules.add(studentSchedule);
        this.schedules.add(oldSchedule);
        
        testInstructor.addCourse(testCourse);
        testInstructor.addCourse(testCourse2);
        testInstructor.addCourse(oldCourse);
        
    }
    
    private String generateUUID()
    {
        UUID id = UUID.randomUUID();
        return id.toString();
    }
    
    @Override
    public HAMKUser authenticateUser(String userID, String password)
    {
        HAMKUser user = getUserByID(userID);
        if (user != null && user.getPassword().equals(password))
        {
            return user;
        }
        
        return null;
    }
    
    @Override
    public HAMKUser getUserByID(String userID)
    {
        for (HAMKUser user : userDatabase)
        {
            if (user.getUserID().equals(userID))
            {
                return user;
            }
        }

        return null;
    }
    
    @Override
    public Course getCourseByCRN(String crn)
    {
        for (Course course : courseCatalog)
        {
            if (course.getCRN().equals(crn))
            {
                return course;
            }
        }
        
        return null;
    }
    
    @Override
    public ArrayList<Schedule> getAllSchedulesForStudent(Student student)
    {
        ArrayList<Schedule> results = new ArrayList<Schedule>();
        for (Schedule s : this.schedules)
        {
            if (s.getStudent().equals(student))
            {
                results.add(s);
            }
        }
        return results;
    }
    
    @Override
    public ArrayList<Course> getCoursesForInstructor(Instructor instructor)
    {
        ArrayList<Course> courses = new ArrayList<Course>();
        for (Course course : courseCatalog)
        {
            if (course.getInstructor().equals(instructor))
            {
                courses.add(course);
            }
        }
        
        return courses;
    }
    
    @Override
    public boolean addCourseToCatalog(Course course)
    {
        String CRN = generateCRN();
        course.setCRN(CRN);
        Instructor instructor = course.getInstructor();
        instructor.addCourse(course);
        courseCatalog.add(course);
        return true;
    }
    
    private String generateCRN()
    {
        boolean isUnique = false;
        Random ran = new Random();
        short LENGTH = 6;
        String CRN = null;
        while (!isUnique)
        {
            CRN = "";
            for (short i = 0; i < LENGTH; i++)
            {
                int r = ran.nextInt(9);
                CRN += Integer.toString(r);
            }

            isUnique = isCRNUnique(CRN);
        }
        
        return CRN;
    }
    
    private boolean isCRNUnique(String CRN)
    {
        for (Course c : courseCatalog)
        {
            if (CRN.equals(c.getCRN()))
            {
                return false;
            }
        }
        
        return true;
    }
    
    @Override
    public ArrayList<Course> doCourseQuery(CourseQueryParams query) {
        ArrayList<Course> results = new ArrayList<Course>();
        for (Course c : courseCatalog)
        {
            if (isMatch(c.getCRN(), query.getCRN()) && isMatch(c.getSemester(), query.getSemester()))
            {
                results.add(c);
            }
            else if (isMatch(c.getDepartment().toString(), query.getDepartment()) && isMatch(c.getSemester(), query.getSemester()))
            {
                results.add(c);
            }
            else if (isMatch(c.getName(), query.getName()) && isMatch(c.getSemester(), query.getSemester()))
            {
                results.add(c);
            }
            else if (isMatch(c.getTimeMet().toString(), query.getTime()) && isMatch(c.getSemester(), query.getSemester()))
            {
                results.add(c);
            }
            else if (isMatch(c.getInstructor().toString(), query.getInstructor()) && isMatch(c.getSemester(), query.getSemester()))
            {
                results.add(c);
            }
        }
        
        return results;
    }
    
    private boolean isMatch(String target, String query)
    {
        if (target != null && query != null)
        {
            return (target.equalsIgnoreCase(query) || target.toLowerCase().contains(query.toLowerCase()));
        }
        
        return false;
    }
    
    @Override
    public boolean removeCourseFromCatalog(Course course) {
        if (courseCatalog.contains(course))
        {
            if (course.getCurrentSize() > 0)
            {
                return false;
            }
            else
            {
                courseCatalog.remove(course);
                return true;
            }
        }
        return false;
    }
    
    @Override
    public String getCurrentSemester()
    {
        return university.getSemester();
    }
    
    @Override
    public ArrayList<Department> getAllDepartments()
    {
        return this.departments;
    }

    @Override
    public Department getDepartment(String deptName) 
    {
        for (Department d : this.departments)
        {
            if (d.getName().equals(deptName))
            {
                return d;
            }
        }
        return null;
    }

    @Override
    public Course getCourseByName(String name) {
        for (Course c : this.courseCatalog)
        {
            if (c.getName().equals(name))
            {
                return c;
            }
        }
        return null;
    }

    @Override
    public boolean addUserToDatabase(HAMKUser user) {
        if (user == null || userDatabase.contains(user))
        {
            return false;
        }
        else
        {
            userDatabase.add(user);
            if (user instanceof Student)
            {
                schedules.add(((Student)user).getSchedule());
            }
            return true;
        }
    }
    
    
}
