/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hamkproject;

/**
 *
 * @author astahlman
 */
public class HAMKErrorNotice {
    
    private String message;

    public HAMKErrorNotice(){}
    
    public HAMKErrorNotice(String message)
    {
        this.message = message;
    }
    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }
    
}
